package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

class threadControllerTests {
    private threadController controller;
    private User user;
    private Thread thread;
    private Post post;
    private List<Post> posts;
    private Vote vote;


    @BeforeEach
    @DisplayName("thread controller test")
    void createThreadTest() {
        Timestamp now = Timestamp.from(Instant.now());

        controller = new threadController();
        user = new User("test user", "testuser@example.com", "Test User", "");
        thread = new Thread(user.getNickname(), now, "forum", "message", "slug", "title", 0);
        post = new Post(user.getNickname(), now, thread.getForum(), thread.getMessage(), 0, 0, false);
        vote = new Vote(user.getNickname(), 1);
        posts = List.of(post);

        thread.setId(123);
    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatePost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.createPosts(Mockito.any(), Mockito.any(), Mockito.any()))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(thread);

            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(user);

                controller.createPost(thread.getSlug(), posts);

                threadDaoMock.verify(() -> ThreadDAO.createPosts(thread, posts, List.of(user)));
            }
        }
    }

    @Test
    @DisplayName("Correct post search test")
    void correctlyReturnPost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.getPosts(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                    .thenAnswer((Answer<List<Post>>) invocation -> List.of(post));
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(thread);

            controller.Posts(thread.getSlug(), 10, null, null, true);

            threadDaoMock.verify(() -> ThreadDAO.getPosts(thread.getId(), 10, null, null, true));
        }
    }

    @Test
    @DisplayName("Correct post update test")
    void correctlyChangePost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(thread);
            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(user);

                controller.change(thread.getSlug(), thread);

                threadDaoMock.verify(() -> ThreadDAO.change(thread, thread));
            }
        }
    }

    @Test
    @DisplayName("Correct thread info test")
    void correctlyGetThreadInfo() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(thread);

            controller.info(thread.getSlug());

            threadDaoMock.verify(() -> ThreadDAO.getThreadBySlug(thread.getSlug()));
        }
    }

    @Test
    @DisplayName("Correct vote create test")
    void correctlyCreateVote() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(thread);
            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(user);

                controller.createVote(thread.getSlug(), vote);

                threadDaoMock.verify(() -> ThreadDAO.createVote(thread, vote));
            }
        }
    }
}
